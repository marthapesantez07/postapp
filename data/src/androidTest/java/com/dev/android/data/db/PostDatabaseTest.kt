package com.dev.android.data.db

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.dev.android.data.db.dao.PostEntityDAO
import com.dev.android.data.db.entities.PostEntity
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.IOException

class PostDatabaseTest {
    private lateinit var postDao: PostEntityDAO
    private lateinit var db: PostDatabase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        // Using an in-memory database because the information stored here disappears when the
        // process is killed.
        db = Room.inMemoryDatabaseBuilder(context, PostDatabase::class.java)
            // Allowing main thread queries, just for testing.
            .allowMainThreadQueries()
            .build()
        postDao = db.postEntityDAO()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertPost() {
        val post = PostEntity(1, 1, "test", "test", favorite = 1)
        postDao.insertTest(post)
        val listPost = postDao.getTestPost()
        assertEquals(listPost.size, 1)
    }

}