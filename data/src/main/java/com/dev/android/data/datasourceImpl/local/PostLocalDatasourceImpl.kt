package com.dev.android.data.datasourceImpl.local

import com.dev.android.data.db.dao.PostEntityDAO
import com.dev.android.data.mappers.toEntity
import com.dev.android.data.mappers.toModel
import com.dev.android.domain.datasource.local.PostLocalDatasource
import com.dev.android.domain.models.PostModel
import com.dev.android.domain.networkObject.PostNetwork
import io.reactivex.Completable
import io.reactivex.Maybe
import javax.inject.Inject

class PostLocalDatasourceImpl @Inject constructor(private val postEntityDAO: PostEntityDAO) :
    PostLocalDatasource {
    override fun savePost(listPost: List<PostNetwork>): Completable {
        return postEntityDAO.insertPost(listPost.map { it.toEntity() })
    }

    override fun getAllPost(): Maybe<List<PostModel>> {
        return postEntityDAO.getAllPost().map {
            it.toModel()
        }
    }

    override fun deleteAll(): Completable {
        return postEntityDAO.deleteAllPost()
    }

    override fun updatePost(postModel: PostModel): Completable {
        return postEntityDAO.updatePost(postModel.toEntity())
    }

    override fun deletePost(postModel: PostModel): Completable {
        return postEntityDAO.deleteOnePost(postModel.toEntity())
    }


}