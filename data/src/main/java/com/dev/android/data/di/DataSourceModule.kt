package com.dev.android.data.di

import com.dev.android.data.apiService.ApiService
import com.dev.android.data.datasourceImpl.local.PostLocalDatasourceImpl
import com.dev.android.data.datasourceImpl.remote.CommentNetworkDatasourceImpl
import com.dev.android.data.datasourceImpl.remote.PostNetworkDatasourceImpl
import com.dev.android.data.datasourceImpl.remote.UserNetworkDatasourceImpl
import com.dev.android.data.db.dao.PostEntityDAO
import com.dev.android.domain.datasource.local.PostLocalDatasource
import com.dev.android.domain.datasource.remote.CommentNetworkDatasource
import com.dev.android.domain.datasource.remote.PostNetworkDatasource
import com.dev.android.domain.datasource.remote.UserNetworkDatasource
import dagger.Module
import dagger.Provides

@Module
class DataSourceModule {

    @Provides
    fun providePostNetworkDataSource(apiService: ApiService): PostNetworkDatasource {
        return PostNetworkDatasourceImpl(apiService)
    }

    @Provides
    fun provideLocalDataSource(postEntityDAO: PostEntityDAO): PostLocalDatasource {
        return PostLocalDatasourceImpl(postEntityDAO)
    }

    @Provides
    fun provideUserNetworkDatasource(apiService: ApiService): UserNetworkDatasource {
        return UserNetworkDatasourceImpl(apiService)
    }

    @Provides
    fun provideCommentNetworkDatasource(apiService: ApiService): CommentNetworkDatasource {
        return CommentNetworkDatasourceImpl(apiService)
    }
}