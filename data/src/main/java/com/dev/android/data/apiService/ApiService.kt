package com.dev.android.data.apiService

import com.dev.android.data.apiService.constants.ApiServiceConstants.Companion.GET_COMMENTS_BY_POST_ID
import com.dev.android.data.apiService.constants.ApiServiceConstants.Companion.GET_POST
import com.dev.android.data.apiService.constants.ApiServiceConstants.Companion.GET_USER_BY_ID
import com.dev.android.domain.networkObject.CommentNetwork
import com.dev.android.domain.networkObject.PostNetwork
import com.dev.android.domain.networkObject.UserNetwork
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET(GET_POST)
    fun getPost(): Single<List<PostNetwork>>

    @GET(GET_USER_BY_ID)
    fun getUserById(@Path("user_id") userId: Int): Single<UserNetwork>

    @GET(GET_COMMENTS_BY_POST_ID)
    fun getCommentsByPostId(@Query("postId") postId: Int): Single<List<CommentNetwork>>
}