package com.dev.android.data.repositoryImpl

import com.dev.android.domain.datasource.remote.CommentNetworkDatasource
import com.dev.android.domain.models.CommentModel
import com.dev.android.domain.repository.CommentRepo
import io.reactivex.Single
import javax.inject.Inject

class CommentRepoImpl @Inject constructor(private val commentNetworkDatasource: CommentNetworkDatasource) :
    CommentRepo {
    override fun getCommentsByPostId(postId: Int): Single<List<CommentModel>> {
        return commentNetworkDatasource.getCommentsByPostId(postId)
    }
}