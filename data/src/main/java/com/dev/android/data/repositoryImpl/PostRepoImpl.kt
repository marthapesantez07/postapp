package com.dev.android.data.repositoryImpl

import android.util.Log
import com.dev.android.domain.datasource.local.PostLocalDatasource
import com.dev.android.domain.datasource.remote.PostNetworkDatasource
import com.dev.android.domain.models.PostModel
import com.dev.android.domain.repository.PostRepo
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PostRepoImpl @Inject constructor(
    private val postNetworkDatasource: PostNetworkDatasource,
    private val postLocalDatasource: PostLocalDatasource
) : PostRepo {

    override fun getAllPost(): Maybe<List<PostModel>> {
        return postLocalDatasource.getAllPost()
    }

    override fun refreshPost(): Completable {
        return postNetworkDatasource.getPostOnline().doOnSuccess {
            postLocalDatasource.savePost(it).observeOn(Schedulers.computation()).subscribe()
        }.doOnError {
            Log.i("PostRepoImpl", "No devuelve post")
        }.toCompletable()
    }

    override fun deleteAll(): Completable {
        return postLocalDatasource.deleteAll()
    }

    override fun updatePost(postModel: PostModel): Completable {
        return postLocalDatasource.updatePost(postModel)
    }

    override fun deleteOnePost(postModel: PostModel): Completable {
        return postLocalDatasource.deletePost(postModel)
    }

}