package com.dev.android.data.di

import com.dev.android.data.repositoryImpl.CommentRepoImpl
import com.dev.android.data.repositoryImpl.PostRepoImpl
import com.dev.android.data.repositoryImpl.UserRepoImpl
import com.dev.android.domain.datasource.local.PostLocalDatasource
import com.dev.android.domain.datasource.remote.CommentNetworkDatasource
import com.dev.android.domain.datasource.remote.PostNetworkDatasource
import com.dev.android.domain.datasource.remote.UserNetworkDatasource
import com.dev.android.domain.repository.CommentRepo
import com.dev.android.domain.repository.PostRepo
import com.dev.android.domain.repository.UserRepo
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {
    @Provides
    fun providePostRepo(
        postNetworkDatasource: PostNetworkDatasource,
        postLocalDatasource: PostLocalDatasource
    ): PostRepo {
        return PostRepoImpl(postNetworkDatasource, postLocalDatasource)
    }

    @Provides
    fun provideUserRepo(userNetworkDatasource: UserNetworkDatasource): UserRepo {
        return UserRepoImpl(userNetworkDatasource)
    }

    @Provides
    fun provideCommentRepo(commentNetworkDatasource: CommentNetworkDatasource): CommentRepo {
        return CommentRepoImpl(commentNetworkDatasource)
    }
}