package com.dev.android.data.db.dao

import androidx.room.*
import com.dev.android.data.db.entities.PostEntity
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
interface PostEntityDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPost(postEntity: List<PostEntity>): Completable

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTest(postEntity: PostEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertOnePost(postEntity: PostEntity): Completable

    @Query("DELETE FROM ${PostEntity.TABLE_NAME}")
    fun deleteAllPost(): Completable

    @Query("SELECT * FROM ${PostEntity.TABLE_NAME} ORDER BY favorite desc")
    fun getAllPost(): Maybe<List<PostEntity>>

    @Query("SELECT * FROM ${PostEntity.TABLE_NAME}")
    fun getTestPost(): List<PostEntity>

    @Update
    fun updatePost(postEntity: PostEntity): Completable

    @Delete
    fun deleteOnePost(postEntity: PostEntity): Completable
}