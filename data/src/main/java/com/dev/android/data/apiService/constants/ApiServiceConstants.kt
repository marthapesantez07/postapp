package com.dev.android.data.apiService.constants

class ApiServiceConstants {
    companion object {
        const val URL_API_SERVICE = "https://jsonplaceholder.typicode.com/"
        const val GET_POST = "posts"
        const val GET_USER_BY_ID = "users/{user_id}"
        const val GET_COMMENTS_BY_POST_ID = "comments"
    }
}