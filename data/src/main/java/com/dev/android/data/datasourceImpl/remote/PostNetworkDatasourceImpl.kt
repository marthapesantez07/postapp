package com.dev.android.data.datasourceImpl.remote

import com.dev.android.data.apiService.ApiService
import com.dev.android.domain.datasource.remote.PostNetworkDatasource
import com.dev.android.domain.networkObject.PostNetwork
import io.reactivex.Single
import javax.inject.Inject

class PostNetworkDatasourceImpl @Inject constructor(private val apiService: ApiService) :
    PostNetworkDatasource {

    override fun getPostOnline(): Single<List<PostNetwork>> {
        return apiService.getPost()
    }
}