package com.dev.android.data.mappers

import com.dev.android.domain.models.CommentModel
import com.dev.android.domain.networkObject.CommentNetwork

fun CommentNetwork.toModel(): CommentModel {
    return CommentModel(
        postId = this.postId,
        id = this.id,
        name = this.name,
        email = this.email,
        body = this.body
    )
}

fun List<CommentNetwork>.toModel(): List<CommentModel> {
    val list = mutableListOf<CommentModel>()
    this.forEach {
        list.add(it.toModel())
    }
    return list
}