package com.dev.android.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dev.android.data.db.dao.PostEntityDAO
import com.dev.android.data.db.entities.CommentEntity
import com.dev.android.data.db.entities.PostEntity
import com.dev.android.data.db.entities.UserEntity

@Database(
    entities = [PostEntity::class, UserEntity::class, CommentEntity::class
    ],
    version = PostDatabase.VERSION, exportSchema = false
)
abstract class PostDatabase : RoomDatabase() {

    abstract fun postEntityDAO(): PostEntityDAO

    companion object {
        const val VERSION = 1
    }
}