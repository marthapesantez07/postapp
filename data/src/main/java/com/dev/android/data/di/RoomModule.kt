package com.dev.android.data.di

import android.app.Application
import androidx.room.Room
import com.dev.android.data.db.PostDatabase
import com.dev.android.data.db.dao.PostEntityDAO
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {
    companion object {
        const val POST_DB_NAME = "POST_DB"
    }

    @Singleton
    @Provides
    fun provideGeneralApplicationDataBase(application: Application): PostDatabase {
        return Room.databaseBuilder(
            application.applicationContext, PostDatabase::class.java,
            POST_DB_NAME
        ).fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun providePostEntityDao(postDatabase: PostDatabase): PostEntityDAO {
        return postDatabase.postEntityDAO()
    }

}