package com.dev.android.data.datasourceImpl.remote

import com.dev.android.data.apiService.ApiService
import com.dev.android.data.mappers.toModel
import com.dev.android.domain.datasource.remote.UserNetworkDatasource
import com.dev.android.domain.models.UserModel
import io.reactivex.Single
import javax.inject.Inject

class UserNetworkDatasourceImpl @Inject constructor(private val apiService: ApiService) :
    UserNetworkDatasource {
    override fun getUserById(id: Int): Single<UserModel> {
        return apiService.getUserById(id).map { it.toModel() }
    }
}