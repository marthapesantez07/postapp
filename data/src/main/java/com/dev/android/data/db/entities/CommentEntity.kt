package com.dev.android.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = CommentEntity.TABLE_NAME)
data class CommentEntity(
    var postId: Int = -1,
    @PrimaryKey
    var id: Int = -1,
    var name: String = "",
    var email: String = "",
    var body: String = ""
) {
    companion object {
        const val TABLE_NAME = "CommentEntity"
    }
}