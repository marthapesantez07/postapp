package com.dev.android.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = UserEntity.TABLE_NAME)
data class UserEntity(
    @PrimaryKey
    var id: Int = -1,
    var username: String = "",
    var email: String = "",
    var phone: String = "",
    var website: String = "",
    var company: String = ""
) {
    companion object {
        const val TABLE_NAME = "UserEntity"
    }
}