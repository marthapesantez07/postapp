package com.dev.android.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = PostEntity.TABLE_NAME)
data class PostEntity(
    @PrimaryKey
    var id: Int = -1,
    var userId: Int = -1,
    var title: String = "",
    var body: String = "",
    var favorite: Int = 0
) {
    companion object {
        const val TABLE_NAME = "PostEntity"
    }
}