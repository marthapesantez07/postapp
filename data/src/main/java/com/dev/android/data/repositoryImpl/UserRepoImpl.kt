package com.dev.android.data.repositoryImpl

import com.dev.android.domain.datasource.remote.UserNetworkDatasource
import com.dev.android.domain.models.UserModel
import com.dev.android.domain.repository.UserRepo
import io.reactivex.Single
import javax.inject.Inject

class UserRepoImpl @Inject constructor(private val userNetworkDatasource: UserNetworkDatasource) :
    UserRepo {
    override fun getUserById(id: Int): Single<UserModel> {
        return userNetworkDatasource.getUserById(id)
    }
}