package com.dev.android.data.mappers

import com.dev.android.domain.models.UserModel
import com.dev.android.domain.networkObject.UserNetwork

fun UserNetwork.toModel(): UserModel {
    return UserModel(
        id = this.id,
        username = this.username,
        email = this.email,
        phone = this.phone,
        website = this.website
    )
}