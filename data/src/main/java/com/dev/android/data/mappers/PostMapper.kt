package com.dev.android.data.mappers

import com.dev.android.data.db.entities.PostEntity
import com.dev.android.domain.models.PostModel
import com.dev.android.domain.networkObject.PostNetwork


fun PostNetwork.toEntity(): PostEntity {
    return PostEntity(
        id = this.id,
        userId = this.userId,
        title = this.title,
        body = this.body,
        favorite = 0
    )
}

fun PostEntity.toModel(): PostModel {
    return PostModel(
        userId = this.userId,
        id = this.id,
        title = this.title,
        body = this.body,
        favorite = this.favorite
    )
}

fun List<PostEntity>.toModel(): List<PostModel> {
    val list = mutableListOf<PostModel>()
    this.forEach {
        list.add(it.toModel())
    }
    return list
}

fun PostModel.toEntity(): PostEntity {
    return PostEntity(
        id = this.id,
        userId = this.userId,
        title = this.title,
        body = this.body,
        favorite = this.favorite
    )
}