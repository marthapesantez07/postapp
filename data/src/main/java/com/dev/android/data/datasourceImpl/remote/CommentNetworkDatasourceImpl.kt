package com.dev.android.data.datasourceImpl.remote

import com.dev.android.data.apiService.ApiService
import com.dev.android.data.mappers.toModel
import com.dev.android.domain.datasource.remote.CommentNetworkDatasource
import com.dev.android.domain.models.CommentModel
import io.reactivex.Single
import javax.inject.Inject

class CommentNetworkDatasourceImpl @Inject constructor(private val apiService: ApiService) :
    CommentNetworkDatasource {
    override fun getCommentsByPostId(postId: Int): Single<List<CommentModel>> {
        return apiService.getCommentsByPostId(postId).map { it.toModel() }
    }
}