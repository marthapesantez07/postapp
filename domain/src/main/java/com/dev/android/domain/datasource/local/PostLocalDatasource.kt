package com.dev.android.domain.datasource.local

import com.dev.android.domain.models.PostModel
import com.dev.android.domain.networkObject.PostNetwork
import io.reactivex.Completable
import io.reactivex.Maybe

interface PostLocalDatasource {
    fun savePost(listPost: List<PostNetwork>): Completable
    fun getAllPost(): Maybe<List<PostModel>>
    fun deleteAll(): Completable
    fun updatePost(postModel: PostModel): Completable
    fun deletePost(postModel: PostModel): Completable
}