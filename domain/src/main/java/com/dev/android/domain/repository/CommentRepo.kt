package com.dev.android.domain.repository

import com.dev.android.domain.models.CommentModel
import io.reactivex.Single

interface CommentRepo {
    fun getCommentsByPostId(postId: Int): Single<List<CommentModel>>
}