package com.dev.android.domain.usecases

import com.dev.android.domain.repository.CommentRepo
import javax.inject.Inject

class GetCommentByPostId @Inject constructor(private val commentRepo: CommentRepo) {
    fun invoke(postId: Int) = commentRepo.getCommentsByPostId(postId)
}