package com.dev.android.domain.repository

import com.dev.android.domain.models.PostModel
import io.reactivex.Completable
import io.reactivex.Maybe

interface PostRepo {
    fun getAllPost(): Maybe<List<PostModel>>
    fun refreshPost(): Completable
    fun deleteAll(): Completable
    fun updatePost(postModel: PostModel): Completable
    fun deleteOnePost(postModel: PostModel): Completable
}