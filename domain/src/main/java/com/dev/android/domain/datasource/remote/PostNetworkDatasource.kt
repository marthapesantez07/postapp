package com.dev.android.domain.datasource.remote

import com.dev.android.domain.networkObject.PostNetwork
import io.reactivex.Single

interface PostNetworkDatasource {
    fun getPostOnline(): Single<List<PostNetwork>>
}