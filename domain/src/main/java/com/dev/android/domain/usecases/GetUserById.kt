package com.dev.android.domain.usecases

import com.dev.android.domain.repository.UserRepo
import javax.inject.Inject

class GetUserById @Inject constructor(private val userRepo: UserRepo) {
    fun invoke(userId: Int) = userRepo.getUserById(userId)
}