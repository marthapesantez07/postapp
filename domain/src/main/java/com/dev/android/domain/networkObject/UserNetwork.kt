package com.dev.android.domain.networkObject

data class UserNetwork(
    val id: Int = -1,
    val username: String = "",
    val email: String = "",
    val phone: String = "",
    val website: String = ""
)