package com.dev.android.domain.models

import java.io.Serializable

data class PostModel(
    var userId: Int = -1,
    var id: Int = -1,
    var title: String = "",
    var body: String = "",
    var favorite: Int = 0
) : Serializable