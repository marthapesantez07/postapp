package com.dev.android.domain.networkObject

data class CommentNetwork(
    val postId: Int = -1,
    val id: Int = -1,
    val name: String = "",
    val email: String = "",
    val body: String = ""
)