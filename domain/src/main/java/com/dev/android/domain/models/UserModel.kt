package com.dev.android.domain.models

data class UserModel(
    val id: Int = -1,
    val username: String = "",
    val email: String = "",
    val phone: String = "",
    val website: String = ""
)