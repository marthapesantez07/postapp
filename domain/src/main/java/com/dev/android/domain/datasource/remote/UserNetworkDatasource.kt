package com.dev.android.domain.datasource.remote

import com.dev.android.domain.models.UserModel
import io.reactivex.Single

interface UserNetworkDatasource {
    fun getUserById(id: Int): Single<UserModel>
}