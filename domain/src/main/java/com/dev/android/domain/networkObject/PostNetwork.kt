package com.dev.android.domain.networkObject

data class PostNetwork(
    val userId: Int = -1,
    val id: Int = -1,
    val title: String = "",
    val body: String = "",
)