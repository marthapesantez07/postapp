package com.dev.android.domain.usecases

import com.dev.android.domain.repository.PostRepo
import javax.inject.Inject

class RefreshPost @Inject constructor(private val postRepo: PostRepo) {
    fun invoke() = postRepo.refreshPost()
}