package com.dev.android.domain.repository

import com.dev.android.domain.models.UserModel
import io.reactivex.Single

interface UserRepo {
    fun getUserById(id: Int): Single<UserModel>
}