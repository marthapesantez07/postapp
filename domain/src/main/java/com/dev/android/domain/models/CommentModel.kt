package com.dev.android.domain.models

data class CommentModel(
    val postId: Int = -1,
    val id: Int = -1,
    val name: String = "",
    val email: String = "",
    val body: String = ""
)