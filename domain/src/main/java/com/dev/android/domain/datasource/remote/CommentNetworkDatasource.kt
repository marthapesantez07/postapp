package com.dev.android.domain.datasource.remote

import com.dev.android.domain.models.CommentModel
import io.reactivex.Single

interface CommentNetworkDatasource {
    fun getCommentsByPostId(postId: Int): Single<List<CommentModel>>
}