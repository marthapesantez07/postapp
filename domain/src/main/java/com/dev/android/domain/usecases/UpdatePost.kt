package com.dev.android.domain.usecases

import com.dev.android.domain.models.PostModel
import com.dev.android.domain.repository.PostRepo
import javax.inject.Inject

class UpdatePost @Inject constructor(private val postRepo: PostRepo) {
    fun invoke(postModel: PostModel) = postRepo.updatePost(postModel)
}