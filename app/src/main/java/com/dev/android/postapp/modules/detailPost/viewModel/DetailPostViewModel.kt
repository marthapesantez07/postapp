package com.dev.android.postapp.modules.detailPost.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dev.android.data.rx.SchedulerProvider
import com.dev.android.domain.models.CommentModel
import com.dev.android.domain.models.PostModel
import com.dev.android.domain.models.UserModel
import com.dev.android.domain.usecases.DeleteOnePost
import com.dev.android.domain.usecases.GetCommentByPostId
import com.dev.android.domain.usecases.GetUserById
import com.dev.android.domain.usecases.UpdatePost
import com.dev.android.postapp.modules.detailPost.view.DetailPostFragmentArgs
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class DetailPostViewModel @Inject constructor(
    private val getUserById: GetUserById,
    private val getCommentByPostId: GetCommentByPostId,
    private val schedulerProvider: SchedulerProvider,
    private val updatePost: UpdatePost,
    private val deleteOnePost: DeleteOnePost
) : ViewModel() {
    private val _resultPost: MutableLiveData<PostModel> = MutableLiveData()

    val resultPost: LiveData<PostModel>
        get() = _resultPost

    private val _resultUser: MutableLiveData<UserModel> = MutableLiveData()
    val resultUser: LiveData<UserModel>
        get() = _resultUser

    private val _resultComments: MutableLiveData<List<CommentModel>> = MutableLiveData()
    val resultComments: LiveData<List<CommentModel>>
        get() = _resultComments

    private val compositeDisposable = CompositeDisposable()

    fun setUp(bundle: DetailPostFragmentArgs) {
        bundle.postModel.let {
            _resultPost.value = it
            getUser(it.userId)
            getComments(it.id)
        }
    }

    private fun getUser(userId: Int) {
        getUserById.invoke(userId)
            .compose(schedulerProvider.getSchedulersForSingle())
            .subscribe({
                Log.i("DetailPost", "getUser ->  $it")
                _resultUser.postValue(it)
            }, {
                Log.e("DetailPost", "getUser -> error $it")
            }).let {
                compositeDisposable.add(it)
            }
    }

    private fun getComments(postId: Int) {
        getCommentByPostId.invoke(postId)
            .compose(schedulerProvider.getSchedulersForSingle())
            .subscribe({
                Log.i("DetailPost", "getComments ->  $it")
                _resultComments.postValue(it)
            }, {
                Log.e("DetailPost", "getComments -> error $it")
            }).let {
                compositeDisposable.add(it)
            }
    }

    fun updateFavoritePost(postModel: PostModel) {
        postModel.favorite = if (postModel.favorite == 1) 0 else 1
        updatePost.invoke(postModel).compose(schedulerProvider.getSchedulersForCompletable())
            .subscribe(
                {
                    Log.e("DetailPost", "updateFavoritePost -> success")
                }, {
                    Log.e("DetailPost", "updateFavoritePost -> error $it")
                }).let {
                compositeDisposable.add(it)
            }
    }

    fun deletePost(postModel: PostModel) {
        deleteOnePost.invoke(postModel).compose(schedulerProvider.getSchedulersForCompletable())
            .subscribe(
                {
                    Log.e("DetailPost", "deletePost -> success")
                }, {
                    Log.e("DetailPost", "deletePost -> error $it")
                }).let {
                compositeDisposable.add(it)
            }
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }
}