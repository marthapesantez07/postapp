package com.dev.android.postapp.di.modules

import com.dev.android.data.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class RxModule {

    @Provides
    @Singleton
    fun provideSchedulerProvider() =
        SchedulerProvider(Schedulers.io(), AndroidSchedulers.mainThread())

    @Provides
    fun provideCompositeDisposable() = CompositeDisposable()

}