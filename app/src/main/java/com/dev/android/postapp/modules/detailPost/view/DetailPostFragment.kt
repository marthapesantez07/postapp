package com.dev.android.postapp.modules.detailPost.view

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dev.android.domain.models.PostModel
import com.dev.android.domain.models.UserModel
import com.dev.android.postapp.R
import com.dev.android.postapp.base.obtainViewModelF
import com.dev.android.postapp.databinding.FragmentDetailPostBinding
import com.dev.android.postapp.modules.detailPost.view.adapter.CommentViewAdapter
import com.dev.android.postapp.modules.detailPost.viewModel.DetailPostViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class DetailPostFragment : DaggerFragment() {
    private lateinit var binding: FragmentDetailPostBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var detailPostViewModel: DetailPostViewModel

    private val args: DetailPostFragmentArgs by navArgs()

    private val commentAdapter: CommentViewAdapter = CommentViewAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailPostBinding.inflate(inflater, container, false)
        detailPostViewModel = viewModelFactory.obtainViewModelF(this)
        detailPostViewModel.setUp(args)
        detailPostViewModel.resultPost.observe(viewLifecycleOwner, {
            it?.let {
                setDescription(it)
            }
        })

        detailPostViewModel.resultUser.observe(viewLifecycleOwner, {
            it?.let {
                setUser(it)
            }
        })
        with(binding) {
            rvComments.apply {
                adapter = commentAdapter
                layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            }

        }

        detailPostViewModel.resultComments.observe(viewLifecycleOwner, {
            it?.let {
                commentAdapter.submitList(it)
            }
        })

        binding.floatingDelete.setOnClickListener {
            deletePost()
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    private fun setDescription(postModel: PostModel) {
        with(binding) {
            txtDescription.text = postModel.body
        }
    }

    private fun setUser(userModel: UserModel) {
        with(binding) {
            txtName.text = getString(R.string.name_user, userModel.username)
            txtEmail.text = getString(R.string.email_user, userModel.email)
            txtPhone.text = getString(R.string.phone_user, userModel.phone)
            txtWebsite.text = getString(R.string.website_user, userModel.website)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_detail, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.give_remove_like -> {
            updateFavoritePost()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun updateFavoritePost() {
        detailPostViewModel.updateFavoritePost(args.postModel)
        findNavController().popBackStack()
    }

    private fun deletePost() {
        detailPostViewModel.deletePost(args.postModel)
        Toast.makeText(
            context,
            "Delete post success",
            Toast.LENGTH_SHORT
        ).show()
        findNavController().popBackStack()
    }
}