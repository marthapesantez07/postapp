package com.dev.android.postapp.di

import android.app.Application
import com.dev.android.data.di.DataSourceModule
import com.dev.android.data.di.NetWorkModule
import com.dev.android.data.di.RepositoryModule
import com.dev.android.data.di.RoomModule
import com.dev.android.postapp.ApplicationClass
import com.dev.android.postapp.di.modules.ActivityBuilderModule
import com.dev.android.postapp.di.modules.AndroidModule
import com.dev.android.postapp.di.modules.RxModule
import com.dev.android.postapp.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilderModule::class,
        ViewModelModule::class,
        NetWorkModule::class,
        AndroidModule::class,
        RxModule::class,
        RoomModule::class,
        DataSourceModule::class,
        RepositoryModule::class,
        AndroidSupportInjectionModule::class,
    ]
)
interface AppComponent : AndroidInjector<DaggerApplication> {
    fun inject(app: ApplicationClass)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

}