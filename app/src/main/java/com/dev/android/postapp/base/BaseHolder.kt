package com.dev.android.postapp.base

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val context: Context by lazy {
        itemView.context
    }
}