package com.dev.android.postapp.modules.listPost.view.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.dev.android.domain.models.PostModel
import com.dev.android.postapp.modules.listPost.view.holder.PostViewHolder
import javax.inject.Inject

class PostViewAdapter @Inject constructor(private val onClickToDescription: (PostModel) -> Unit) :
    ListAdapter<PostModel, PostViewHolder>(PostDiffCallback()) {
    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        getItem(position)?.let { holder.binding(it, onClickToDescription) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        return PostViewHolder.createViewHolder(parent)
    }
}

class PostDiffCallback : DiffUtil.ItemCallback<PostModel>() {
    override fun areItemsTheSame(oldItem: PostModel, newItem: PostModel): Boolean {
        return oldItem.id == newItem.id

    }

    override fun areContentsTheSame(oldItem: PostModel, newItem: PostModel): Boolean {
        return oldItem == newItem
    }

}