package com.dev.android.postapp.di.modules

import com.dev.android.postapp.annotation.PerActivity
import com.dev.android.postapp.modules.detailPost.view.DetailPostFragment
import com.dev.android.postapp.modules.listPost.view.ListPostFragment
import com.dev.android.postapp.modules.main.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {
    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun provideMainActivity(): MainActivity

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun providePostFragment(): ListPostFragment

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun provideDetailPostFragment(): DetailPostFragment

}