package com.dev.android.postapp.modules.listPost.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dev.android.data.rx.SchedulerProvider
import com.dev.android.domain.models.PostModel
import com.dev.android.domain.usecases.DeleteAllPost
import com.dev.android.domain.usecases.GetPost
import com.dev.android.domain.usecases.RefreshPost
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ListPostViewModel @Inject constructor(
    private val getPost: GetPost,
    private val refreshPost: RefreshPost,
    private val schedulerProvider: SchedulerProvider,
    private val deleteAllPost: DeleteAllPost
) : ViewModel() {
    private var _navigateToDescriptionMLD: MutableLiveData<PostModel> = MutableLiveData()
    val navigateToDescription: LiveData<PostModel>
        get() = _navigateToDescriptionMLD

    private var _isRefresh: MutableLiveData<Boolean> = MutableLiveData(false)
    val isRefresh: LiveData<Boolean>
        get() = _isRefresh

    private val _listPost = MutableLiveData<List<PostModel>>()
    val listPost: LiveData<List<PostModel>>
        get() = _listPost

    private var _showProgressBarMLD: MutableLiveData<Boolean> = MutableLiveData(false)
    val showProgressBar: LiveData<Boolean>
        get() = _showProgressBarMLD

    private val compositeDisposable = CompositeDisposable()

    fun refreshPost() {
        _showProgressBarMLD.value = true
        refreshPost.invoke().compose(schedulerProvider.getSchedulersForCompletable()).subscribe(
            {
                Log.e("ListPost", "refreshPost -> success")
                getAllPost(true)
            }, {
                Log.e("ListPost", "refreshPost -> error $it")
                _showProgressBarMLD.value = false
            }).let {
            compositeDisposable.add(it)
        }
    }

    fun getFavorites() {
        val post = _listPost.value
        _listPost.value = post?.filter { it.favorite == 1 }
    }

    fun getAllPost(online: Boolean) {
        if (!online) {
            _showProgressBarMLD.value = true
        }
        getPost.invoke()
            .compose(schedulerProvider.getSchedulersForMaybe())
            .subscribe({
                Log.i("ListPost", "getAllPost ->  $it")
                _listPost.postValue(it)
                _showProgressBarMLD.value = false
            }, {
                Log.e("ListPost", "getAllPost -> error $it")
                _showProgressBarMLD.value = false
            }, {
                Log.e("ListPost", "getAllPost -> data not found")
                _showProgressBarMLD.value = false
            }).let {
                compositeDisposable.add(it)
            }
    }

    fun deleteAll() {
        _showProgressBarMLD.value = true
        deleteAllPost.invoke().compose(schedulerProvider.getSchedulersForCompletable()).subscribe(
            {
                _isRefresh.postValue(true)
                Log.e("ListPost", "deleteAll -> success")
            }, {
                Log.e("ListPost", "deleteAll -> error $it")
                _showProgressBarMLD.value = false
            }).let {
            compositeDisposable.add(it)
        }
    }

    fun goToDescription(post: PostModel) {
        _navigateToDescriptionMLD.value = post
        Log.e("ListViewPost", "navigateToDescription ->  $post")
    }

    fun navigationComplete() {
        _navigateToDescriptionMLD.value = null
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }
}