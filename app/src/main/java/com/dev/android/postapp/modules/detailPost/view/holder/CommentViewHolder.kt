package com.dev.android.postapp.modules.detailPost.view.holder

import android.view.LayoutInflater
import android.view.ViewGroup
import com.dev.android.domain.models.CommentModel
import com.dev.android.postapp.base.BaseHolder
import com.dev.android.postapp.databinding.ItemCommentBinding
import javax.inject.Inject

class CommentViewHolder @Inject constructor(private val binding: ItemCommentBinding) :
    BaseHolder(binding.root) {

    fun binding(
        item: CommentModel,
    ) {
        binding.txtComment.text = item.body
    }

    companion object {
        fun createViewHolder(parent: ViewGroup): CommentViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return CommentViewHolder(ItemCommentBinding.inflate(inflater, parent, false))
        }

    }
}