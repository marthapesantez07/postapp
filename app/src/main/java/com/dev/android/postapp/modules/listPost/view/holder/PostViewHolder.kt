package com.dev.android.postapp.modules.listPost.view.holder

import android.view.LayoutInflater
import android.view.ViewGroup
import com.dev.android.domain.models.PostModel
import com.dev.android.postapp.R
import com.dev.android.postapp.base.BaseHolder
import com.dev.android.postapp.databinding.ItemPostBinding
import javax.inject.Inject

class PostViewHolder @Inject constructor(private val binding: ItemPostBinding) :
    BaseHolder(binding.root) {

    fun binding(
        item: PostModel,
        onClickToDescription: (PostModel) -> Unit
    ) {
        binding.txtTitle.text = item.title
        binding.txtTitle.setOnClickListener { onClickToDescription(item) }
        if (item.favorite == 1) {
            binding.imgFavorite.setImageResource(R.drawable.ic_start_favorite)
        } else {
            binding.imgFavorite.setImageResource(0)
        }
    }

    companion object {
        fun createViewHolder(parent: ViewGroup): PostViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            return PostViewHolder(ItemPostBinding.inflate(inflater, parent, false))
        }

    }
}