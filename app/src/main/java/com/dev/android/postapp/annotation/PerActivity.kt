package com.dev.android.postapp.annotation

import javax.inject.Scope


@Scope
internal annotation class PerActivity
