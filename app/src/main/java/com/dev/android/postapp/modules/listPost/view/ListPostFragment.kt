package com.dev.android.postapp.modules.listPost.view

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dev.android.domain.models.PostModel
import com.dev.android.postapp.R
import com.dev.android.postapp.base.obtainViewModelF
import com.dev.android.postapp.databinding.FragmentListPostBinding
import com.dev.android.postapp.modules.listPost.view.adapter.PostViewAdapter
import com.dev.android.postapp.modules.listPost.viewModel.ListPostViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class ListPostFragment : DaggerFragment() {

    private lateinit var binding: FragmentListPostBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var listPostViewModel: ListPostViewModel

    private val postAdapter: PostViewAdapter by lazy {
        PostViewAdapter { post ->
            listPostViewModel.goToDescription(post)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListPostBinding.inflate(inflater, container, false)
        listPostViewModel = viewModelFactory.obtainViewModelF(this)
        binding.group.isSelectionRequired = true
        binding.optionAll.isChecked = true
        listPostViewModel.getAllPost(false)
        binding.group.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.option_all -> {
                    listPostViewModel.getAllPost(false)
                }
                R.id.option_favorite -> {
                    listPostViewModel.getFavorites()
                }
            }
        }


        with(binding) {
            rvListPost.apply {
                adapter = postAdapter
                layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

            }

        }
        binding.floatingDelete.setOnClickListener {
            deleteAllPost()
        }
        observeObject()
        setHasOptionsMenu(true)
        return binding.root
    }

    private fun observeObject() {
        //observe refresh post
        listPostViewModel.isRefresh.observe(viewLifecycleOwner, {
            if (it == true) {
                // getPostOnline()
            }
        })

        listPostViewModel.navigateToDescription.observe(viewLifecycleOwner, {
            it?.let {
                navigateToDescription(it)
                listPostViewModel.navigationComplete()
            }
        })

        listPostViewModel.listPost.observe(viewLifecycleOwner, {
            it?.let {
                postAdapter.submitList(it)
            }
        })

        listPostViewModel.showProgressBar.observe(viewLifecycleOwner, {
            it?.let {
                hideProgressBar(it)
            }
        })

    }

    private fun deleteAllPost() {
        listPostViewModel.deleteAll()
        Toast.makeText(
            context,
            "Delete all success",
            Toast.LENGTH_SHORT
        ).show()
        listPostViewModel.getAllPost(false)
    }

    private fun hideProgressBar(showPB: Boolean) {
        if (showPB) {
            binding.pbLoading.visibility = View.VISIBLE
            binding.clList.visibility = View.GONE
        } else {
            binding.pbLoading.visibility = View.GONE
            binding.clList.visibility = View.VISIBLE
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.refresh -> {
            listPostViewModel.refreshPost()
            binding.optionAll.isChecked = true
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun navigateToDescription(postModel: PostModel) {
        findNavController().safeNavigate(
            ListPostFragmentDirections.actionListPostFragmentToDetailPostFragment(postModel)
        )
    }

    private fun NavController.safeNavigate(direction: NavDirections) {
        currentDestination?.getAction(direction.actionId)?.run {
            navigate(direction)
        }
    }

}