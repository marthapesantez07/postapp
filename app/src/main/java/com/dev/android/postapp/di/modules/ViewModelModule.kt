package com.dev.android.postapp.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dev.android.postapp.annotation.ViewModelKey
import com.dev.android.postapp.base.ViewModelFactory
import com.dev.android.postapp.modules.detailPost.viewModel.DetailPostViewModel
import com.dev.android.postapp.modules.listPost.viewModel.ListPostViewModel
import com.dev.android.postapp.modules.main.viewModel.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
abstract class ViewModelModule {
    @Binds
    @Singleton
    abstract fun provideViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun provideMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListPostViewModel::class)
    abstract fun provideListPostViewModel(listMovieViewModel: ListPostViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailPostViewModel::class)
    abstract fun provideDetailPostViewModel(detailPostViewModel: DetailPostViewModel): ViewModel
}